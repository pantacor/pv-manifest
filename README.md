
# Pantavisor Build System

## How to Clone this Repository

```
git clone --recursive https://gitlab.com/<group-name or username>/pv-manifest.git
```

## Build automatically

Push a new tag to trigger rpi3-builder job to build a new image (stable).

Trigger from schedule to update release.xml and trigger rpi3-builder job to build a new image (latest).

## Build manually

To build a release, checkout the release tag on this repo and then
build with release.xml manifest, like:

```
repo init -m release.xml; repo sync
```

Also, following [link](https://docs.pantahub.com/) has instructions on how to use Pantavisor and associated tools.
